﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace YizitCodeCreater
{
    public partial class FormTnsName : Form
    {
        public FormTnsName()
        {
            InitializeComponent();
        }

        private void BTCancl_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void BTSave_Click(object sender, EventArgs e)
        {
            string name = TBTnsOracleName.Text;
            string host = Form1.save_host;
            string port = Form1.save_port;
            string ser_name = Form1.save_name;
            
            string saveNew = "\n\n" + name.Trim() + @" =
  (DESCRIPTION =
    (ADDRESS_LIST =
      (ADDRESS = (PROTOCOL = TCP)(HOST = " + host .Trim()+ @")(PORT = " + port.Trim()+ @"))
    )
    (CONNECT_DATA =
      (SERVER = DEDICATED)
      (SERVICE_NAME = " + ser_name.Trim()+@")
    )
  )";
            Encoding encoder = Encoding.UTF8;
            byte[] bytes = encoder.GetBytes(saveNew);
            if (TBTnsOracleName.Text == "")
            {
                MessageBox.Show("请输入保存名称");
            }
            try
            {
                FileStream fs = File.OpenWrite(Form1.tns_path);
                //设定书写的开始位置为文件的末尾  
                fs.Position = fs.Length;
                //将待写入内容追加到文件末尾  
                try
                {

                    fs.Write(bytes, 0, bytes.Length);
                    this.Close();
                }
                catch (Exception ex)
                {
                    Console.WriteLine("文件打开失败{0}", ex.ToString());
                }
                finally
                {
                    fs.Close();
                }
            }
            catch { }
            
        }
    }
}
