﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Reflection;
using System.Text;

namespace YizitCodeCreater
{
    public static class CreatSddOracle
    {
        public static DataTable NewSddTable(DataTable dt, string TableName)
        {
            AppDomain.CurrentDomain.AssemblyResolve += (sender, senderArgs) =>
            {
                // 当前程序集
                var executingAssembly = Assembly.GetExecutingAssembly();
                // 当前程序集名称
                var assemblyName = new AssemblyName(executingAssembly.FullName).Name;
                // dll名称
                var dllName = new AssemblyName(senderArgs.Name).Name;
                // 待加载dll路径，指向当前程序集资源文件中dll路径。* 根据程序结构调整，使其正确指向dll
                var dllUri = assemblyName + "." + dllName + ".dll";
                // 加载dll
                using (var resourceStream = executingAssembly.GetManifestResourceStream(dllUri))
                {
                    var assemblyData = new Byte[resourceStream.Length];
                    resourceStream.Read(assemblyData, 0, assemblyData.Length);
                    return Assembly.Load(assemblyData); //加载dll
                }
            };
            DataSet ds = new DataSet();
            DataTable dtResult = new DataTable();
            string[] sheetname = new string[1];
            dtResult.Columns.Add("Name", typeof(string));
            dtResult.Columns.Add("Type", typeof(string));
            dtResult.Columns.Add("DefaultValue", typeof(string));
            dtResult.Columns.Add("PubPrv", typeof(string));
            dtResult.Columns.Add("Get", typeof(string));
            dtResult.Columns.Add("Set", typeof(string));
            dtResult.Columns.Add("intablename", typeof(string));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string Name = dt.Rows[i][0].ToString().ToLower();
                Name = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Name);
                string Type = dt.Rows[i][1].ToString();
                switch (Type)
                {
                    case "VARCHAR2":
                        Type = "string";
                        break;
                    case "CHAR":
                        Type = "string";
                        break;
                    case "DATE":
                        Type = "DateTime";
                        break;
                    case "NUMBER":
                        Type = "decimal";
                        break;
                }
                string getset = "m_" + Name;
                DataRow dr = dtResult.NewRow();
                dr["Name"] = Name;
                dr["Type"] = Type;
                dr["Get"] = getset;
                dr["Set"] = getset;
                dr["intablename"] = dt.Rows[i][0].ToString();
                dtResult.Rows.Add(dr);
            }
            ds.Tables.Add(dtResult);
            sheetname[0] = dt.Rows[0][0].ToString();
            Datatable2Excel.Datatable2Excel.ModifExcel(Form1.basePath, TableName + ".xls", ds, sheetname, false);
            return dtResult ;
        }
    }
}
