﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace YizitCodeCreater
{
    public static class CreatMap
    {
        public static string NewMap(Hashtable ht, DataTable dtTable, DataTable dtTableInfo, DataTable dtTableP)
        {
            string Name = dtTableInfo.Rows[0][0].ToString();

            string MapStart = @"<?xml version=" + "\"" + "1.0" + "\"" + " encoding=" + "\"" + "utf-8" + "\"" + @"?>
<map>";
            string template1 = "\n" + @"  <class name=" + "\"";
            if (ht["group1"].ToString() == "1")
            {
                Name = Name.ToLower();
                Name = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(Name);
                int index = 0;
                index = Name.IndexOf("_");
                Name = Name.Remove(0, index + 1);
                index = Name.LastIndexOf("_");
                Name = Name.Remove(index, Name.Length - index);
                Name = Name.Replace("_", "");
                Name = Name + "Object";
                template1 += Name;
            }
            else if (ht["group1"].ToString() == "2")
            {
                Name = Name.Replace("_", "").ToUpper();
                template1 += Name;
            }
            else if (ht["group1"].ToString() == "3")
            {
                Name = dtTableInfo.Rows[0][1].ToString();
                template1 += Name;
            }
            template1 += "\"" + @" table=" + "\"" + ht["username"].ToString() + "." + dtTableInfo.Rows[0][0].ToString() + "\"" + " database=" + "\"" + ht["oraclename"].ToString() + "\">\n";


            for (int i = 0; i < dtTable.Rows.Count; i++)
            {
                string temp1 = dtTable.Rows[i][0].ToString().ToLower();
                
                string comment = "";
                if (ht["group2"].ToString() == "1")
                {
                    temp1 = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(temp1);
                }
                else
                {
                    temp1 = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(temp1).Replace("_","");
                }
                if (dtTable.Rows[i][0].ToString() == dtTableP.Rows[0][1].ToString())
                {
                    comment = @" key=" + "\"" + "primary" + "\"" + " increment=" + "\"" + ht["cid"].ToString() + "\"";
                }
                if (ht["group3"].ToString() == "2")
                {
                    template1 += @"    <attribute name=" + "\"" + temp1 + "\"" + @" column=" + "\"" + dtTable.Rows[i][0].ToString() + "\"" + " type=" + "\"" + dtTable.Rows[i][1].ToString() + "\"" + comment + "/>" + "\n";
                }
                else
                {
                    template1 += @"    <attribute name=" + "\"" + temp1 + "\"" + @" column=" + "\"" + dtTable.Rows[i][0].ToString() + "\"" + " headText=" + "\"" + dtTable.Rows[i][3].ToString() + "\"" + " type=" + "\"" + dtTable.Rows[i][1].ToString() + "\"" + comment + "/>" + "\n";
                }
            }
            string MapEnd = @"  </class>" + "\n" + "</map>";

            return MapStart + template1 + MapEnd;
        }
    }
}
