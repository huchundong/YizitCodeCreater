﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace YizitCodeCreater
{
    public static class CreatDic
    {
        public static string NewDic(Hashtable ht, DataTable dt, DataTable dtTableInfo)
        {
            string DicStart = @"  <Table name=" + "\"" + dtTableInfo.Rows[0][0].ToString() + "\"" + @">" + "\n";
            string DicEnd = @"  </Table>";
            string Template1 = @"     <column fieldName=" + "\"" + "MasterTitle" + "\"" + " headText=" + "\"" + dtTableInfo.Rows[0][1].ToString() + "\"" + "/>";
            string Template2 = "\n";
            string comment = "";


            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("temp1", typeof(string));
            //dtResult.Columns.Add("temp2", typeof(string));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string temp1 = dt.Rows[i][0].ToString().ToLower();
                temp1 = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(temp1);
                //string temp2 = "m_" + temp1;
                DataRow dr = dtResult.NewRow();
                dr["temp1"] = temp1;
                //dr["temp2"] = temp2;
                dtResult.Rows.Add(dr);
                if (dt.Rows[i]["comment"] != null)
                {
                    comment = dt.Rows[i]["comment"].ToString();
                }
                Template2 += @"     <column fieldName=" + "\"" + temp1 + "\"" + " headText=" + "\"" + comment + "\"" + "/>" + "\n";
            }
            return DicStart + Template1 + Template2 + DicEnd;
        }
    }
}
