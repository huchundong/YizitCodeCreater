﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;


namespace YizitCodeCreater
{
    public static class CreatObject
    {
        public static string NewObject(Hashtable ht, DataTable dt)
        {
            string ObjectStart = "#region\n/*\nCreated By Yizit-Code-Creater\nYizit-Code-Creater BuildInfo\nVersion 1.1\nDate " + DateTime.Now.ToString()+ "\n*/\n#endregion\n\nusing System;\nusing System.Collections.Generic;\nusing System.Text;" + "\nusing "
                               + ht["PersistenceLayerName"] + ";\n"
                               + "\nnamespace " + ht["RuleName"]
                               + "\n{\n\tpublic class " + ht["ObjectName"] + " :" + ht["baseObjectName"] + "\n\t{";
            string ObjectEnd = "\n\t}\n}";
            //预处理字符串，转换为首字母大写和添加m_
            DataTable dtResult = new DataTable();
            dtResult.Columns.Add("temp1", typeof(string));
            dtResult.Columns.Add("temp2", typeof(string));
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string temp1 = dt.Rows[i][0].ToString().ToLower();
                temp1 = System.Globalization.CultureInfo.CurrentCulture.TextInfo.ToTitleCase(temp1);
                string temp2 = "m_" + temp1;
                DataRow dr = dtResult.NewRow();
                dr["temp1"] = temp1;
                dr["temp2"] = temp2;
                dtResult.Rows.Add(dr);
            }

            string template1 = "";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                string temp = dtResult.Rows[i]["temp1"].ToString();
                string comment = "";
                if (dt.Rows[i]["comment"] != null)
                {
                    comment = "  /*" + dt.Rows[i]["comment"].ToString() + @"*/";
                }
                template1 += "\n\t\tpublic const string " + dt.Rows[i][0].ToString() + " = \"" + temp + "\";" + comment;
            }

            /*private Decimal m_Deleted_User_Id = Decimal.MinValue;
            private DateTime m_Created_Time;
            private DateTime m_Modified_Time;
            private String m_Deleted_Flag;*/
            string template2 = "\n";
            string template3 = "\n";
            for (int i = 0; i < dt.Rows.Count; i++)
            {
                if (dt.Rows[i]["type"].ToString() == "NUMBER")
                {
                    template2 += "\n\t\tprivate Decimal " + dtResult.Rows[i]["temp2"] + "= Decimal.MinValue;";
                    template3 += "\n\t\tpublic Decimal " + dtResult.Rows[i]["temp1"] + "\n\t\t{\n\t\t\tget\n\t\t\t{\n\t\t\t\treturn "
                              + dtResult.Rows[i]["temp2"] + ";\n\t\t\t}\n\t\t\tset\n\t\t\t{\n\t\t\t\t" + dtResult.Rows[i]["temp2"]
                              + " = value;\n\t\t\t}\n\t\t}";
                }
                else if (dt.Rows[i]["type"].ToString() == "VARCHAR2" || dt.Rows[i]["type"].ToString() == "CHAR")
                {
                    template2 += "\n\t\tprivate String " + dtResult.Rows[i]["temp2"] + ";";
                    template3 += "\n\t\tpublic String " + dtResult.Rows[i]["temp1"] + "\n\t\t{\n\t\t\tget\n\t\t\t{\n\t\t\t\treturn "
                              + dtResult.Rows[i]["temp2"] + ";\n\t\t\t}\n\t\t\tset\n\t\t\t{\n\t\t\t\t" + dtResult.Rows[i]["temp2"]
                              + " = value;\n\t\t\t}\n\t\t}";
                }
                else if (dt.Rows[i]["type"].ToString() == "DATE")
                {
                    template2 += "\n\t\tprivate DateTime " + dtResult.Rows[i]["temp2"] + ";";
                    template3 += "\n\t\tpublic DateTime " + dtResult.Rows[i]["temp1"] + "\n\t\t{\n\t\t\tget\n\t\t\t{\n\t\t\t\treturn "
                              + dtResult.Rows[i]["temp2"] + ";\n\t\t\t}\n\t\t\tset\n\t\t\t{\n\t\t\t\t" + dtResult.Rows[i]["temp2"]
                              + " = value;\n\t\t\t}\n\t\t}";
                }
            }

            return ObjectStart + template1 + template2 + template3 + ObjectEnd;
            
        }
    }
}
