﻿namespace YizitCodeCreater
{
    partial class Form1
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(Form1));
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.关于ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.yizitCodeCreaterToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.hhuCunDongToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.BTConnect = new System.Windows.Forms.Button();
            this.RTBObject = new System.Windows.Forms.RichTextBox();
            this.LBName = new System.Windows.Forms.Label();
            this.TBName = new System.Windows.Forms.TextBox();
            this.LBPassword = new System.Windows.Forms.Label();
            this.TBPassword = new System.Windows.Forms.TextBox();
            this.LBIP = new System.Windows.Forms.Label();
            this.TBIp = new System.Windows.Forms.TextBox();
            this.LBPort = new System.Windows.Forms.Label();
            this.TBPort = new System.Windows.Forms.TextBox();
            this.LBServerName = new System.Windows.Forms.Label();
            this.TBServerName = new System.Windows.Forms.TextBox();
            this.treeTables = new System.Windows.Forms.TreeView();
            this.LBPersistenceLayerName = new System.Windows.Forms.Label();
            this.TBPersistenceLayer = new System.Windows.Forms.TextBox();
            this.LBRuleName = new System.Windows.Forms.Label();
            this.TBRuleName = new System.Windows.Forms.TextBox();
            this.LBBaseObject = new System.Windows.Forms.Label();
            this.LBObjectName = new System.Windows.Forms.Label();
            this.TBBaseObject = new System.Windows.Forms.TextBox();
            this.TBObjectName = new System.Windows.Forms.TextBox();
            this.BTCreatObject = new System.Windows.Forms.Button();
            this.tabControl1 = new System.Windows.Forms.TabControl();
            this.tabClassMap = new System.Windows.Forms.TabPage();
            this.BTRecreate = new System.Windows.Forms.Button();
            this.BTCopyMap = new System.Windows.Forms.Button();
            this.richTextBoxMap = new System.Windows.Forms.RichTextBox();
            this.TBOracleName = new System.Windows.Forms.TextBox();
            this.TBCid = new System.Windows.Forms.TextBox();
            this.checkBoxOracleName = new System.Windows.Forms.CheckBox();
            this.checkBoxCid = new System.Windows.Forms.CheckBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.radioButton6 = new System.Windows.Forms.RadioButton();
            this.radioButton7 = new System.Windows.Forms.RadioButton();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.radioButton4 = new System.Windows.Forms.RadioButton();
            this.radioButton5 = new System.Windows.Forms.RadioButton();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.radioButton3 = new System.Windows.Forms.RadioButton();
            this.radioButton2 = new System.Windows.Forms.RadioButton();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.tabObjectCreat = new System.Windows.Forms.TabPage();
            this.listViewSdd = new System.Windows.Forms.ListView();
            this.BTSdd = new System.Windows.Forms.Button();
            this.BTCopy = new System.Windows.Forms.Button();
            this.tabDic = new System.Windows.Forms.TabPage();
            this.BTCopyDic = new System.Windows.Forms.Button();
            this.BTCreatDic = new System.Windows.Forms.Button();
            this.richTextBoxDic = new System.Windows.Forms.RichTextBox();
            this.BTTns = new System.Windows.Forms.Button();
            this.comboBox1 = new System.Windows.Forms.ComboBox();
            this.BTAddTns = new System.Windows.Forms.Button();
            this.menuStrip1.SuspendLayout();
            this.tabControl1.SuspendLayout();
            this.tabClassMap.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.tabObjectCreat.SuspendLayout();
            this.tabDic.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.关于ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(1366, 25);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 关于ToolStripMenuItem
            // 
            this.关于ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.yizitCodeCreaterToolStripMenuItem,
            this.hhuCunDongToolStripMenuItem});
            this.关于ToolStripMenuItem.Name = "关于ToolStripMenuItem";
            this.关于ToolStripMenuItem.Size = new System.Drawing.Size(44, 21);
            this.关于ToolStripMenuItem.Text = "关于";
            // 
            // yizitCodeCreaterToolStripMenuItem
            // 
            this.yizitCodeCreaterToolStripMenuItem.Name = "yizitCodeCreaterToolStripMenuItem";
            this.yizitCodeCreaterToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.yizitCodeCreaterToolStripMenuItem.Text = "Yizit-Code-Creater";
            // 
            // hhuCunDongToolStripMenuItem
            // 
            this.hhuCunDongToolStripMenuItem.Name = "hhuCunDongToolStripMenuItem";
            this.hhuCunDongToolStripMenuItem.Size = new System.Drawing.Size(183, 22);
            this.hhuCunDongToolStripMenuItem.Text = "HuCunDong";
            // 
            // BTConnect
            // 
            this.BTConnect.Location = new System.Drawing.Point(778, 28);
            this.BTConnect.Name = "BTConnect";
            this.BTConnect.Size = new System.Drawing.Size(75, 23);
            this.BTConnect.TabIndex = 1;
            this.BTConnect.Text = "连接数据库";
            this.BTConnect.UseVisualStyleBackColor = true;
            this.BTConnect.Click += new System.EventHandler(this.BTConnect_Click);
            // 
            // RTBObject
            // 
            this.RTBObject.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.RTBObject.Location = new System.Drawing.Point(6, 77);
            this.RTBObject.Name = "RTBObject";
            this.RTBObject.Size = new System.Drawing.Size(1342, 474);
            this.RTBObject.TabIndex = 2;
            this.RTBObject.Text = "";
            // 
            // LBName
            // 
            this.LBName.AutoSize = true;
            this.LBName.Location = new System.Drawing.Point(166, 31);
            this.LBName.Name = "LBName";
            this.LBName.Size = new System.Drawing.Size(41, 12);
            this.LBName.TabIndex = 5;
            this.LBName.Text = "用户名";
            // 
            // TBName
            // 
            this.TBName.Location = new System.Drawing.Point(213, 28);
            this.TBName.Name = "TBName";
            this.TBName.Size = new System.Drawing.Size(100, 21);
            this.TBName.TabIndex = 6;
            this.TBName.Text = "cmes";
            // 
            // LBPassword
            // 
            this.LBPassword.AutoSize = true;
            this.LBPassword.Location = new System.Drawing.Point(319, 31);
            this.LBPassword.Name = "LBPassword";
            this.LBPassword.Size = new System.Drawing.Size(29, 12);
            this.LBPassword.TabIndex = 7;
            this.LBPassword.Text = "密码";
            // 
            // TBPassword
            // 
            this.TBPassword.Location = new System.Drawing.Point(354, 28);
            this.TBPassword.Name = "TBPassword";
            this.TBPassword.PasswordChar = '*';
            this.TBPassword.Size = new System.Drawing.Size(100, 21);
            this.TBPassword.TabIndex = 8;
            this.TBPassword.Text = "cmes";
            // 
            // LBIP
            // 
            this.LBIP.AutoSize = true;
            this.LBIP.Location = new System.Drawing.Point(460, 31);
            this.LBIP.Name = "LBIP";
            this.LBIP.Size = new System.Drawing.Size(41, 12);
            this.LBIP.TabIndex = 9;
            this.LBIP.Text = "IP地址";
            // 
            // TBIp
            // 
            this.TBIp.Location = new System.Drawing.Point(508, 27);
            this.TBIp.Name = "TBIp";
            this.TBIp.Size = new System.Drawing.Size(100, 21);
            this.TBIp.TabIndex = 10;
            this.TBIp.Text = "192.168.1.20";
            // 
            // LBPort
            // 
            this.LBPort.AutoSize = true;
            this.LBPort.Location = new System.Drawing.Point(614, 31);
            this.LBPort.Name = "LBPort";
            this.LBPort.Size = new System.Drawing.Size(29, 12);
            this.LBPort.TabIndex = 11;
            this.LBPort.Text = "端口";
            // 
            // TBPort
            // 
            this.TBPort.Location = new System.Drawing.Point(661, 28);
            this.TBPort.Name = "TBPort";
            this.TBPort.Size = new System.Drawing.Size(100, 21);
            this.TBPort.TabIndex = 12;
            this.TBPort.Text = "1521";
            // 
            // LBServerName
            // 
            this.LBServerName.AutoSize = true;
            this.LBServerName.Location = new System.Drawing.Point(12, 31);
            this.LBServerName.Name = "LBServerName";
            this.LBServerName.Size = new System.Drawing.Size(41, 12);
            this.LBServerName.TabIndex = 13;
            this.LBServerName.Text = "服务名";
            // 
            // TBServerName
            // 
            this.TBServerName.Location = new System.Drawing.Point(59, 27);
            this.TBServerName.Name = "TBServerName";
            this.TBServerName.Size = new System.Drawing.Size(100, 21);
            this.TBServerName.TabIndex = 14;
            this.TBServerName.Text = "ynms";
            // 
            // treeTables
            // 
            this.treeTables.BackColor = System.Drawing.SystemColors.ActiveCaption;
            this.treeTables.Font = new System.Drawing.Font("MS Reference Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.treeTables.ForeColor = System.Drawing.SystemColors.InfoText;
            this.treeTables.HideSelection = false;
            this.treeTables.LineColor = System.Drawing.Color.DarkOrange;
            this.treeTables.Location = new System.Drawing.Point(12, 84);
            this.treeTables.Name = "treeTables";
            this.treeTables.Size = new System.Drawing.Size(195, 554);
            this.treeTables.TabIndex = 15;
            this.treeTables.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.treeTables_AfterSelect);
            // 
            // LBPersistenceLayerName
            // 
            this.LBPersistenceLayerName.AutoSize = true;
            this.LBPersistenceLayerName.Location = new System.Drawing.Point(6, 15);
            this.LBPersistenceLayerName.Name = "LBPersistenceLayerName";
            this.LBPersistenceLayerName.Size = new System.Drawing.Size(77, 12);
            this.LBPersistenceLayerName.TabIndex = 16;
            this.LBPersistenceLayerName.Text = "持久层空间名";
            // 
            // TBPersistenceLayer
            // 
            this.TBPersistenceLayer.Location = new System.Drawing.Point(89, 12);
            this.TBPersistenceLayer.Name = "TBPersistenceLayer";
            this.TBPersistenceLayer.Size = new System.Drawing.Size(205, 21);
            this.TBPersistenceLayer.TabIndex = 17;
            this.TBPersistenceLayer.Text = "YizitMes.PersistenceLayer";
            // 
            // LBRuleName
            // 
            this.LBRuleName.AutoSize = true;
            this.LBRuleName.Location = new System.Drawing.Point(8, 43);
            this.LBRuleName.Name = "LBRuleName";
            this.LBRuleName.Size = new System.Drawing.Size(77, 12);
            this.LBRuleName.TabIndex = 18;
            this.LBRuleName.Text = "实体层空间名";
            // 
            // TBRuleName
            // 
            this.TBRuleName.Location = new System.Drawing.Point(89, 39);
            this.TBRuleName.Name = "TBRuleName";
            this.TBRuleName.Size = new System.Drawing.Size(205, 21);
            this.TBRuleName.TabIndex = 19;
            this.TBRuleName.Text = "Yizit.BusinessRule";
            // 
            // LBBaseObject
            // 
            this.LBBaseObject.AutoSize = true;
            this.LBBaseObject.Location = new System.Drawing.Point(299, 15);
            this.LBBaseObject.Name = "LBBaseObject";
            this.LBBaseObject.Size = new System.Drawing.Size(29, 12);
            this.LBBaseObject.TabIndex = 20;
            this.LBBaseObject.Text = "父类";
            // 
            // LBObjectName
            // 
            this.LBObjectName.AutoSize = true;
            this.LBObjectName.Location = new System.Drawing.Point(299, 43);
            this.LBObjectName.Name = "LBObjectName";
            this.LBObjectName.Size = new System.Drawing.Size(65, 12);
            this.LBObjectName.TabIndex = 21;
            this.LBObjectName.Text = "实体类名称";
            // 
            // TBBaseObject
            // 
            this.TBBaseObject.Location = new System.Drawing.Point(382, 12);
            this.TBBaseObject.Name = "TBBaseObject";
            this.TBBaseObject.Size = new System.Drawing.Size(172, 21);
            this.TBBaseObject.TabIndex = 22;
            this.TBBaseObject.Text = "EntityObject";
            // 
            // TBObjectName
            // 
            this.TBObjectName.Location = new System.Drawing.Point(382, 39);
            this.TBObjectName.Name = "TBObjectName";
            this.TBObjectName.Size = new System.Drawing.Size(172, 21);
            this.TBObjectName.TabIndex = 23;
            this.TBObjectName.Text = "hucdObject";
            // 
            // BTCreatObject
            // 
            this.BTCreatObject.Location = new System.Drawing.Point(561, 12);
            this.BTCreatObject.Name = "BTCreatObject";
            this.BTCreatObject.Size = new System.Drawing.Size(75, 21);
            this.BTCreatObject.TabIndex = 24;
            this.BTCreatObject.Text = "生成实体类";
            this.BTCreatObject.UseVisualStyleBackColor = true;
            this.BTCreatObject.Click += new System.EventHandler(this.BTCreatObject_Click);
            // 
            // tabControl1
            // 
            this.tabControl1.Controls.Add(this.tabClassMap);
            this.tabControl1.Controls.Add(this.tabObjectCreat);
            this.tabControl1.Controls.Add(this.tabDic);
            this.tabControl1.Location = new System.Drawing.Point(213, 55);
            this.tabControl1.Name = "tabControl1";
            this.tabControl1.SelectedIndex = 0;
            this.tabControl1.Size = new System.Drawing.Size(1362, 583);
            this.tabControl1.SizeMode = System.Windows.Forms.TabSizeMode.Fixed;
            this.tabControl1.TabIndex = 25;
            // 
            // tabClassMap
            // 
            this.tabClassMap.Controls.Add(this.BTRecreate);
            this.tabClassMap.Controls.Add(this.BTCopyMap);
            this.tabClassMap.Controls.Add(this.richTextBoxMap);
            this.tabClassMap.Controls.Add(this.TBOracleName);
            this.tabClassMap.Controls.Add(this.TBCid);
            this.tabClassMap.Controls.Add(this.checkBoxOracleName);
            this.tabClassMap.Controls.Add(this.checkBoxCid);
            this.tabClassMap.Controls.Add(this.groupBox3);
            this.tabClassMap.Controls.Add(this.groupBox2);
            this.tabClassMap.Controls.Add(this.groupBox1);
            this.tabClassMap.Location = new System.Drawing.Point(4, 22);
            this.tabClassMap.Name = "tabClassMap";
            this.tabClassMap.Size = new System.Drawing.Size(1354, 557);
            this.tabClassMap.TabIndex = 2;
            this.tabClassMap.Text = "映射信息";
            this.tabClassMap.UseVisualStyleBackColor = true;
            // 
            // BTRecreate
            // 
            this.BTRecreate.Location = new System.Drawing.Point(528, 65);
            this.BTRecreate.Name = "BTRecreate";
            this.BTRecreate.Size = new System.Drawing.Size(100, 35);
            this.BTRecreate.TabIndex = 11;
            this.BTRecreate.Text = "重新生成";
            this.BTRecreate.UseVisualStyleBackColor = true;
            this.BTRecreate.Click += new System.EventHandler(this.BTRecreate_Click);
            // 
            // BTCopyMap
            // 
            this.BTCopyMap.Location = new System.Drawing.Point(399, 64);
            this.BTCopyMap.Name = "BTCopyMap";
            this.BTCopyMap.Size = new System.Drawing.Size(100, 36);
            this.BTCopyMap.TabIndex = 10;
            this.BTCopyMap.Text = "复制到剪贴板";
            this.BTCopyMap.UseVisualStyleBackColor = true;
            this.BTCopyMap.Click += new System.EventHandler(this.BTCopyMap_Click);
            // 
            // richTextBoxMap
            // 
            this.richTextBoxMap.Location = new System.Drawing.Point(4, 106);
            this.richTextBoxMap.Name = "richTextBoxMap";
            this.richTextBoxMap.Size = new System.Drawing.Size(1347, 448);
            this.richTextBoxMap.TabIndex = 9;
            this.richTextBoxMap.Text = "";
            // 
            // TBOracleName
            // 
            this.TBOracleName.Location = new System.Drawing.Point(528, 37);
            this.TBOracleName.Name = "TBOracleName";
            this.TBOracleName.Size = new System.Drawing.Size(100, 21);
            this.TBOracleName.TabIndex = 8;
            // 
            // TBCid
            // 
            this.TBCid.Location = new System.Drawing.Point(399, 38);
            this.TBCid.Name = "TBCid";
            this.TBCid.Size = new System.Drawing.Size(100, 21);
            this.TBCid.TabIndex = 7;
            this.TBCid.Text = "cid";
            // 
            // checkBoxOracleName
            // 
            this.checkBoxOracleName.AutoSize = true;
            this.checkBoxOracleName.Location = new System.Drawing.Point(528, 16);
            this.checkBoxOracleName.Name = "checkBoxOracleName";
            this.checkBoxOracleName.Size = new System.Drawing.Size(84, 16);
            this.checkBoxOracleName.TabIndex = 6;
            this.checkBoxOracleName.Text = "数据库名称";
            this.checkBoxOracleName.UseVisualStyleBackColor = true;
            this.checkBoxOracleName.CheckedChanged += new System.EventHandler(this.checkBoxOracleName_CheckedChanged);
            // 
            // checkBoxCid
            // 
            this.checkBoxCid.AutoSize = true;
            this.checkBoxCid.Checked = true;
            this.checkBoxCid.CheckState = System.Windows.Forms.CheckState.Checked;
            this.checkBoxCid.Location = new System.Drawing.Point(399, 16);
            this.checkBoxCid.Name = "checkBoxCid";
            this.checkBoxCid.Size = new System.Drawing.Size(72, 16);
            this.checkBoxCid.TabIndex = 5;
            this.checkBoxCid.Text = "序列名称";
            this.checkBoxCid.UseVisualStyleBackColor = true;
            this.checkBoxCid.CheckedChanged += new System.EventHandler(this.checkBoxCid_CheckedChanged);
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.radioButton6);
            this.groupBox3.Controls.Add(this.radioButton7);
            this.groupBox3.Location = new System.Drawing.Point(270, 7);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(106, 93);
            this.groupBox3.TabIndex = 4;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "标题文字";
            // 
            // radioButton6
            // 
            this.radioButton6.AutoSize = true;
            this.radioButton6.Checked = true;
            this.radioButton6.Location = new System.Drawing.Point(7, 21);
            this.radioButton6.Name = "radioButton6";
            this.radioButton6.Size = new System.Drawing.Size(71, 16);
            this.radioButton6.TabIndex = 2;
            this.radioButton6.TabStop = true;
            this.radioButton6.Text = "字段注释";
            this.radioButton6.UseVisualStyleBackColor = true;
            this.radioButton6.CheckedChanged += new System.EventHandler(this.radioButton6_CheckedChanged);
            // 
            // radioButton7
            // 
            this.radioButton7.AutoSize = true;
            this.radioButton7.Location = new System.Drawing.Point(6, 65);
            this.radioButton7.Name = "radioButton7";
            this.radioButton7.Size = new System.Drawing.Size(59, 16);
            this.radioButton7.TabIndex = 1;
            this.radioButton7.Text = "不创建";
            this.radioButton7.UseVisualStyleBackColor = true;
            this.radioButton7.CheckedChanged += new System.EventHandler(this.radioButton7_CheckedChanged);
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.radioButton4);
            this.groupBox2.Controls.Add(this.radioButton5);
            this.groupBox2.Location = new System.Drawing.Point(137, 7);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(106, 93);
            this.groupBox2.TabIndex = 3;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "属性名";
            // 
            // radioButton4
            // 
            this.radioButton4.AutoSize = true;
            this.radioButton4.Checked = true;
            this.radioButton4.Location = new System.Drawing.Point(7, 21);
            this.radioButton4.Name = "radioButton4";
            this.radioButton4.Size = new System.Drawing.Size(71, 16);
            this.radioButton4.TabIndex = 2;
            this.radioButton4.TabStop = true;
            this.radioButton4.Text = "字段名称";
            this.radioButton4.UseVisualStyleBackColor = true;
            this.radioButton4.CheckedChanged += new System.EventHandler(this.radioButton4_CheckedChanged);
            // 
            // radioButton5
            // 
            this.radioButton5.AutoSize = true;
            this.radioButton5.Location = new System.Drawing.Point(6, 65);
            this.radioButton5.Name = "radioButton5";
            this.radioButton5.Size = new System.Drawing.Size(71, 16);
            this.radioButton5.TabIndex = 1;
            this.radioButton5.Text = "字段缩写";
            this.radioButton5.UseVisualStyleBackColor = true;
            this.radioButton5.CheckedChanged += new System.EventHandler(this.radioButton5_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.radioButton3);
            this.groupBox1.Controls.Add(this.radioButton2);
            this.groupBox1.Controls.Add(this.radioButton1);
            this.groupBox1.Location = new System.Drawing.Point(4, 7);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(106, 93);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "类名";
            // 
            // radioButton3
            // 
            this.radioButton3.AutoSize = true;
            this.radioButton3.Location = new System.Drawing.Point(7, 65);
            this.radioButton3.Name = "radioButton3";
            this.radioButton3.Size = new System.Drawing.Size(71, 16);
            this.radioButton3.TabIndex = 2;
            this.radioButton3.Text = "表 注 释";
            this.radioButton3.UseVisualStyleBackColor = true;
            this.radioButton3.CheckedChanged += new System.EventHandler(this.radioButton3_CheckedChanged);
            // 
            // radioButton2
            // 
            this.radioButton2.AutoSize = true;
            this.radioButton2.Location = new System.Drawing.Point(7, 43);
            this.radioButton2.Name = "radioButton2";
            this.radioButton2.Size = new System.Drawing.Size(71, 16);
            this.radioButton2.TabIndex = 1;
            this.radioButton2.Text = "表名缩写";
            this.radioButton2.UseVisualStyleBackColor = true;
            this.radioButton2.CheckedChanged += new System.EventHandler(this.radioButton2_CheckedChanged);
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Checked = true;
            this.radioButton1.Location = new System.Drawing.Point(7, 21);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(71, 16);
            this.radioButton1.TabIndex = 0;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "实体类名";
            this.radioButton1.UseVisualStyleBackColor = true;
            this.radioButton1.CheckedChanged += new System.EventHandler(this.radioButton1_CheckedChanged);
            // 
            // tabObjectCreat
            // 
            this.tabObjectCreat.Controls.Add(this.listViewSdd);
            this.tabObjectCreat.Controls.Add(this.BTSdd);
            this.tabObjectCreat.Controls.Add(this.BTCopy);
            this.tabObjectCreat.Controls.Add(this.RTBObject);
            this.tabObjectCreat.Controls.Add(this.BTCreatObject);
            this.tabObjectCreat.Controls.Add(this.TBRuleName);
            this.tabObjectCreat.Controls.Add(this.TBObjectName);
            this.tabObjectCreat.Controls.Add(this.LBPersistenceLayerName);
            this.tabObjectCreat.Controls.Add(this.TBBaseObject);
            this.tabObjectCreat.Controls.Add(this.TBPersistenceLayer);
            this.tabObjectCreat.Controls.Add(this.LBObjectName);
            this.tabObjectCreat.Controls.Add(this.LBRuleName);
            this.tabObjectCreat.Controls.Add(this.LBBaseObject);
            this.tabObjectCreat.Location = new System.Drawing.Point(4, 22);
            this.tabObjectCreat.Name = "tabObjectCreat";
            this.tabObjectCreat.Padding = new System.Windows.Forms.Padding(3);
            this.tabObjectCreat.Size = new System.Drawing.Size(1354, 557);
            this.tabObjectCreat.TabIndex = 0;
            this.tabObjectCreat.Text = "实体类";
            this.tabObjectCreat.UseVisualStyleBackColor = true;
            // 
            // listViewSdd
            // 
            this.listViewSdd.Location = new System.Drawing.Point(6, 77);
            this.listViewSdd.Name = "listViewSdd";
            this.listViewSdd.Size = new System.Drawing.Size(1342, 477);
            this.listViewSdd.TabIndex = 27;
            this.listViewSdd.UseCompatibleStateImageBehavior = false;
            // 
            // BTSdd
            // 
            this.BTSdd.Location = new System.Drawing.Point(651, 11);
            this.BTSdd.Name = "BTSdd";
            this.BTSdd.Size = new System.Drawing.Size(75, 23);
            this.BTSdd.TabIndex = 26;
            this.BTSdd.Text = "SDD表格";
            this.BTSdd.UseVisualStyleBackColor = true;
            this.BTSdd.Click += new System.EventHandler(this.BTSdd_Click);
            // 
            // BTCopy
            // 
            this.BTCopy.Location = new System.Drawing.Point(561, 40);
            this.BTCopy.Name = "BTCopy";
            this.BTCopy.Size = new System.Drawing.Size(75, 23);
            this.BTCopy.TabIndex = 25;
            this.BTCopy.Text = "复制到剪贴板";
            this.BTCopy.UseVisualStyleBackColor = true;
            this.BTCopy.Click += new System.EventHandler(this.BTCopy_Click);
            // 
            // tabDic
            // 
            this.tabDic.Controls.Add(this.BTCopyDic);
            this.tabDic.Controls.Add(this.BTCreatDic);
            this.tabDic.Controls.Add(this.richTextBoxDic);
            this.tabDic.Location = new System.Drawing.Point(4, 22);
            this.tabDic.Name = "tabDic";
            this.tabDic.Padding = new System.Windows.Forms.Padding(3);
            this.tabDic.Size = new System.Drawing.Size(1354, 557);
            this.tabDic.TabIndex = 1;
            this.tabDic.Text = "数据字典";
            this.tabDic.UseVisualStyleBackColor = true;
            // 
            // BTCopyDic
            // 
            this.BTCopyDic.Location = new System.Drawing.Point(561, 36);
            this.BTCopyDic.Name = "BTCopyDic";
            this.BTCopyDic.Size = new System.Drawing.Size(75, 23);
            this.BTCopyDic.TabIndex = 26;
            this.BTCopyDic.Text = "复制到剪贴板";
            this.BTCopyDic.UseVisualStyleBackColor = true;
            this.BTCopyDic.Click += new System.EventHandler(this.BTCopyDic_Click);
            // 
            // BTCreatDic
            // 
            this.BTCreatDic.Location = new System.Drawing.Point(561, 7);
            this.BTCreatDic.Name = "BTCreatDic";
            this.BTCreatDic.Size = new System.Drawing.Size(75, 23);
            this.BTCreatDic.TabIndex = 4;
            this.BTCreatDic.Text = "生成字典";
            this.BTCreatDic.UseVisualStyleBackColor = true;
            this.BTCreatDic.Click += new System.EventHandler(this.BTCreatDic_Click);
            // 
            // richTextBoxDic
            // 
            this.richTextBoxDic.Font = new System.Drawing.Font("微软雅黑", 10.5F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.richTextBoxDic.Location = new System.Drawing.Point(6, 80);
            this.richTextBoxDic.Name = "richTextBoxDic";
            this.richTextBoxDic.Size = new System.Drawing.Size(1342, 474);
            this.richTextBoxDic.TabIndex = 3;
            this.richTextBoxDic.Text = "";
            // 
            // BTTns
            // 
            this.BTTns.Location = new System.Drawing.Point(113, 55);
            this.BTTns.Name = "BTTns";
            this.BTTns.Size = new System.Drawing.Size(94, 23);
            this.BTTns.TabIndex = 26;
            this.BTTns.Text = "配置TNS路径";
            this.BTTns.UseVisualStyleBackColor = true;
            this.BTTns.Click += new System.EventHandler(this.BTTns_Click);
            // 
            // comboBox1
            // 
            this.comboBox1.FormattingEnabled = true;
            this.comboBox1.Location = new System.Drawing.Point(14, 55);
            this.comboBox1.Name = "comboBox1";
            this.comboBox1.Size = new System.Drawing.Size(93, 20);
            this.comboBox1.TabIndex = 27;
            this.comboBox1.SelectedIndexChanged += new System.EventHandler(this.comboBox1_SelectedIndexChanged);
            // 
            // BTAddTns
            // 
            this.BTAddTns.Location = new System.Drawing.Point(868, 28);
            this.BTAddTns.Name = "BTAddTns";
            this.BTAddTns.Size = new System.Drawing.Size(75, 23);
            this.BTAddTns.TabIndex = 28;
            this.BTAddTns.Text = "添加到TNS";
            this.BTAddTns.UseVisualStyleBackColor = true;
            this.BTAddTns.Click += new System.EventHandler(this.BTAddTns_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1366, 650);
            this.Controls.Add(this.BTAddTns);
            this.Controls.Add(this.comboBox1);
            this.Controls.Add(this.BTTns);
            this.Controls.Add(this.tabControl1);
            this.Controls.Add(this.treeTables);
            this.Controls.Add(this.TBServerName);
            this.Controls.Add(this.LBServerName);
            this.Controls.Add(this.TBPort);
            this.Controls.Add(this.LBPort);
            this.Controls.Add(this.TBIp);
            this.Controls.Add(this.LBIP);
            this.Controls.Add(this.TBPassword);
            this.Controls.Add(this.LBPassword);
            this.Controls.Add(this.TBName);
            this.Controls.Add(this.LBName);
            this.Controls.Add(this.BTConnect);
            this.Controls.Add(this.menuStrip1);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.Text = "因致信息 源码生成工具";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.Form1_FormClosed);
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.tabControl1.ResumeLayout(false);
            this.tabClassMap.ResumeLayout(false);
            this.tabClassMap.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.tabObjectCreat.ResumeLayout(false);
            this.tabObjectCreat.PerformLayout();
            this.tabDic.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 关于ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem yizitCodeCreaterToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem hhuCunDongToolStripMenuItem;
        private System.Windows.Forms.Button BTConnect;
        private System.Windows.Forms.RichTextBox RTBObject;
        private System.Windows.Forms.Label LBName;
        private System.Windows.Forms.TextBox TBName;
        private System.Windows.Forms.Label LBPassword;
        private System.Windows.Forms.TextBox TBPassword;
        private System.Windows.Forms.Label LBIP;
        private System.Windows.Forms.TextBox TBIp;
        private System.Windows.Forms.Label LBPort;
        private System.Windows.Forms.TextBox TBPort;
        private System.Windows.Forms.Label LBServerName;
        private System.Windows.Forms.TextBox TBServerName;
        public System.Windows.Forms.TreeView treeTables;
        private System.Windows.Forms.Label LBPersistenceLayerName;
        private System.Windows.Forms.TextBox TBPersistenceLayer;
        private System.Windows.Forms.Label LBRuleName;
        private System.Windows.Forms.TextBox TBRuleName;
        private System.Windows.Forms.Label LBBaseObject;
        private System.Windows.Forms.Label LBObjectName;
        private System.Windows.Forms.TextBox TBBaseObject;
        private System.Windows.Forms.TextBox TBObjectName;
        private System.Windows.Forms.Button BTCreatObject;
        private System.Windows.Forms.TabPage tabObjectCreat;
        private System.Windows.Forms.TabPage tabDic;
        private System.Windows.Forms.Button BTCopy;
        private System.Windows.Forms.Button BTTns;
        private System.Windows.Forms.ComboBox comboBox1;
        private System.Windows.Forms.Button BTCopyDic;
        private System.Windows.Forms.Button BTCreatDic;
        private System.Windows.Forms.RichTextBox richTextBoxDic;
        private System.Windows.Forms.Button BTAddTns;
        private System.Windows.Forms.TabPage tabClassMap;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton radioButton3;
        private System.Windows.Forms.RadioButton radioButton2;
        private System.Windows.Forms.RadioButton radioButton1;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.RadioButton radioButton6;
        private System.Windows.Forms.RadioButton radioButton7;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.RadioButton radioButton4;
        private System.Windows.Forms.RadioButton radioButton5;
        private System.Windows.Forms.RichTextBox richTextBoxMap;
        private System.Windows.Forms.TextBox TBOracleName;
        private System.Windows.Forms.TextBox TBCid;
        private System.Windows.Forms.CheckBox checkBoxOracleName;
        private System.Windows.Forms.CheckBox checkBoxCid;
        private System.Windows.Forms.Button BTCopyMap;
        private System.Windows.Forms.Button BTRecreate;
        public System.Windows.Forms.TabControl tabControl1;
        private System.Windows.Forms.Button BTSdd;
        public System.Windows.Forms.ListView listViewSdd;
    }
}

