﻿namespace YizitCodeCreater
{
    partial class FormTnsName
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.BTSave = new System.Windows.Forms.Button();
            this.BTCancl = new System.Windows.Forms.Button();
            this.LBSave = new System.Windows.Forms.Label();
            this.TBTnsOracleName = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // BTSave
            // 
            this.BTSave.Location = new System.Drawing.Point(33, 155);
            this.BTSave.Name = "BTSave";
            this.BTSave.Size = new System.Drawing.Size(75, 23);
            this.BTSave.TabIndex = 0;
            this.BTSave.Text = "保存";
            this.BTSave.UseVisualStyleBackColor = true;
            this.BTSave.Click += new System.EventHandler(this.BTSave_Click);
            // 
            // BTCancl
            // 
            this.BTCancl.Location = new System.Drawing.Point(154, 155);
            this.BTCancl.Name = "BTCancl";
            this.BTCancl.Size = new System.Drawing.Size(75, 23);
            this.BTCancl.TabIndex = 1;
            this.BTCancl.Text = "取消";
            this.BTCancl.UseVisualStyleBackColor = true;
            this.BTCancl.Click += new System.EventHandler(this.BTCancl_Click);
            // 
            // LBSave
            // 
            this.LBSave.AutoSize = true;
            this.LBSave.Font = new System.Drawing.Font("宋体", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(134)));
            this.LBSave.Location = new System.Drawing.Point(12, 50);
            this.LBSave.Name = "LBSave";
            this.LBSave.Size = new System.Drawing.Size(96, 16);
            this.LBSave.TabIndex = 2;
            this.LBSave.Text = "TNS连接名称";
            // 
            // TBTnsOracleName
            // 
            this.TBTnsOracleName.Location = new System.Drawing.Point(110, 50);
            this.TBTnsOracleName.Name = "TBTnsOracleName";
            this.TBTnsOracleName.Size = new System.Drawing.Size(140, 21);
            this.TBTnsOracleName.TabIndex = 3;
            // 
            // FormTnsName
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(274, 197);
            this.Controls.Add(this.TBTnsOracleName);
            this.Controls.Add(this.LBSave);
            this.Controls.Add(this.BTCancl);
            this.Controls.Add(this.BTSave);
            this.Name = "FormTnsName";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "保存当前TNS";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button BTSave;
        private System.Windows.Forms.Button BTCancl;
        private System.Windows.Forms.Label LBSave;
        private System.Windows.Forms.TextBox TBTnsOracleName;
    }
}